#!/bin/sh

if ! [[ -f id_rsa ]]; then
    ssh-keygen -f id_rsa

    cp -prv id_rsa* headnode
    cp -prv id_rsa* computenode
fi

printf "building centos7 base system with systemd..\n"
#https://hub.docker.com/_/centos/
(cd systemd; docker build --rm -t local/c7-systemd .)

printf "building hortonworks slave\n"
(cd computenode; sudo docker build -t rayburgemeestre/hortonworks-slave .)

printf "building hortonworks master\n"
(cd headnode; sudo docker build -t rayburgemeestre/hortonworks-master .)

