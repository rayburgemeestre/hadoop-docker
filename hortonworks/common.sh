#!/bin/bash

function launch_master
{
	printf "launching master node...\n"
	echo screen -dmS $1 /bin/sh -c "sudo docker run --net=$2 --name=$1 -h $1 $(echo $3) --privileged -t -i -v /sys/fs/cgroup:/sys/fs/cgroup rayburgemeestre/hortonworks-master"
	screen -dmS $1 /bin/sh -c "sudo docker run --net=$2 --name=$1 -h $1 $(echo $3) --privileged -t -i -v /sys/fs/cgroup:/sys/fs/cgroup rayburgemeestre/hortonworks-master"
}

function launch_node
{
    printf "launching compute node...\n"
	screen -dmS $1 /bin/sh -c "sudo docker run --net=$2 --name=$1 -h $1 $(echo $3) --privileged -t -i -v /sys/fs/cgroup:/sys/fs/cgroup rayburgemeestre/hortonworks-slave"
}

