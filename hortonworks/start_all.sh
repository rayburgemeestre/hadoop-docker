#!/bin/bash

typeset master=horton
typeset network=bridge

. common.sh

launch_master $master $network "-p 8080:8080 -p 8440:8440 -p 8441:8441"
sleep 20
screen -ls; docker ps 
sleep 5
#
## the sleep in between prevents race conditions with docker writing the /etc/hosts file!
#
launch_node node101 $network "-p 50070:50070 -p 16010:16010"
sleep 5
launch_node node102 $network "-p 8088:8088"
sleep 5
launch_node node103 $network
#sleep 5
##launch_node node104 $network
#sleep 5
#launch_node node105 $network
#sleep 5
#launch_node node106 $network
#sleep 5
#
sleep 10
#
screen -ls; docker ps
