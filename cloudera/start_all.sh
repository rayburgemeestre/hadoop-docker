#!/bin/bash

typeset master=cloudera
typeset network=mynet

docker network rm $network; sleep 2
docker network create --driver bridge $network; sleep 2

. common.sh

docker pull rayburgemeestre/cloudera-master:5
docker pull rayburgemeestre/cloudera-slave:5

launch_master $master $network "-p 7180:7180"
sleep 2
screen -ls; docker ps

sleep 20

# the sleep in between prevents race conditions with docker writing the /etc/hosts file!

launch_node node001 $network "-p 8888:8888"
sleep 5
launch_node node002 $network
sleep 5
launch_node node003 $network
sleep 5
#launch_node node004 $network
#sleep 5
#launch_node node005 $network
#sleep 5
#launch_node node006 $network
#sleep 5

sleep 10

screen -ls; docker ps

