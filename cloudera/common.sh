#docker network rm $network; sleep 2
#docker network create --driver bridge $network; sleep 2

function launch_master
{
	printf "launching cloudera node...\n"
	echo screen -dmS $1 /bin/sh -c "sudo docker run --net=$2 --name=$1 -h $1.$network $(echo $3) --privileged -t -i -v /sys/fs/cgroup:/sys/fs/cgroup rayburgemeestre/cloudera-master:5"
	screen -dmS $1 /bin/sh -c "sudo docker run --net=$2 --name=$1 -h $1.$network $(echo $3) --privileged -t -i -v /sys/fs/cgroup:/sys/fs/cgroup rayburgemeestre/cloudera-master:5"
}

function launch_node
{
    printf "launching compute node...\n"
    echo screen -dmS $1 /bin/sh -c "sudo docker run --net=$2 --name=$1 -h $1.$network $(echo $3) --privileged -t -i -v /sys/fs/cgroup:/sys/fs/cgroup rayburgemeestre/cloudera-slave:5"
    screen -dmS $1 /bin/sh -c "sudo docker run --net=$2 --name=$1 -h $1.$network $(echo $3) --privileged -t -i -v /sys/fs/cgroup:/sys/fs/cgroup rayburgemeestre/cloudera-slave:5"
}

