#!/bin/sh

printf "building centos7 base system with systemd..\n"
#https://hub.docker.com/_/centos/
#(cd systemd; docker build --no-cache=true --rm -t local/c7-systemd .)
(cd systemd; docker build --rm -t local/c7-systemd .)

printf "building cloudera slave\n"
(cd computenode; sudo docker build -t rayburgemeestre/cloudera-slave .)

printf "building cloudera master\n"
(cd headnode; sudo docker build -t rayburgemeestre/cloudera-master .)

